<?php

namespace Drupal\search_api_solr_dynamic_connector\Plugin\SolrConnector;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\search_api_solr\SearchApiSolrException;
use Drupal\search_api_solr\SolrConnector\SolrConnectorPluginBase;
use Solarium\Client;
use Solarium\Core\Client\Endpoint;
use Solarium\Core\Client\Request;
use Solarium\Core\Client\Response;
use Solarium\Core\Query\Helper;
use Solarium\Core\Query\QueryInterface;
use Solarium\QueryType\Extract\Result as ExtractResult;
use Solarium\QueryType\Select\Query\Query;
use Solarium\QueryType\Update\Query\Query as UpdateQuery;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Dynamic Solr connector.
 *
 * @SolrConnector(
 *   id = "dynamic_solr_connector",
 *   label = @Translation("Dynamic Solr Connector"),
 *   description = @Translation("A Solr connector that changes depending on environment variables")
 * )
 */
class DynamicSolrConnector extends SolrConnectorPluginBase {

  /**
   * Either a reference to another Solr Connector or NULL if none was found.
   *
   * @var \Drupal\search_api_solr\SolrConnectorInterface|null
   */
  protected $connector;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, $connector) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->connector = $connector;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $connector = NULL;
    $connector = self::loadConnectorFromEnv($container, $configuration);
    return new static($configuration, $plugin_id, $plugin_definition, $connector);
  }

  /**
   * Search for a Solr connector to load if the specific ENV variable is found.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   * @param array $configuration
   *   The configuration of the current connector.
   *
   * @return \Drupal\search_api_solr\SolrConnectorInterface|null
   *   A connection to solr speficied by another server. Or NULL if no
   *   environment variable matched with the criteria specified in the plugin
   *   settings.
   */
  protected static function loadConnectorFromEnv(ContainerInterface $container, array $configuration) {
    $connectors = preg_split("(\r\n?|\n)", $configuration['solr_selector']);
    if (empty($connectors)) {
      return;
    }
    $connect_to = NULL;
    foreach ($connectors as $rule) {
      if (strpos($rule, ':') === FALSE) {
        continue;
      }
      list($env_variable, $server) = explode(':', $rule);
      if (!empty($_ENV[trim($env_variable)])) {
        $connect_to = trim($server);
        break;
      }
    }

    if (empty($connect_to)) {
      return;
    }
    $server = $container
      ->get('entity_type.manager')
      ->getStorage('search_api_server')
      ->load($connect_to);

    if (empty($server)) {
      return;
    }

    if ($server->getBackendId() != 'search_api_solr') {
      return;
    }

    $connector_config = $server->getBackendConfig()['connector_config'];

    // Prevent an infinite loop.
    if (!empty($connector_config['solr_selector'])) {
      return;
    }

    $connector_config = $server->getBackendConfig()['connector_config'];
    $connector_plugin_id = $server->getBackendConfig()['connector'];
    $connector_config = $server->getBackendConfig()['connector_config'];
    $connector = $container
      ->get('plugin.manager.search_api_solr.connector')
      ->createInstance($connector_plugin_id, $connector_config);

    return $connector;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'solr_selector' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['solr_selector'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Selection rules'),
      '#description' => $this->t('Specify ENVIRONMENT_VARIABLE:server_name to connect to solr using the server_name (solr server machine name) when ENVIRONMENT_VARIABLE_NAME is found in the current environment. Place multiple rules in separate lines. The first match found is the one used. If using Pantheon, search for PANTHEON_ENV. For ddev you may want to try DDEV_PRIMARY_URL.'),
      '#default_value' => $this->configuration['solr_selector'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration = $form_state->getValues();
  }

  /**
   * {@inheritdoc}
   */
  public function getServerLink() {
    if (empty($this->connector)) {
      return Link::createFromRoute('', '<none>');
    }
    return $this->connector->getServerLink();
  }

  /**
   * {@inheritdoc}
   */
  public function getCoreLink() {
    if (empty($this->connector)) {
      return Link::createFromRoute('', '<none>');
    }
    return $this->connector->getCoreLink();
  }

  /**
   * {@inheritdoc}
   */
  public function getSolrVersion($force_auto_detect = FALSE) {
    if (empty($this->connector)) {
      return '';
    }
    return $this->connector->getSolrVersion($force_auto_detect = FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getSolrMajorVersion($version = '') {
    if (empty($this->connector)) {
      return 0;
    }
    return $this->connector->getSolrMajorVersion($version);
  }

  /**
   * {@inheritdoc}
   */
  public function getSolrBranch($version = '') {
    if (empty($this->connector)) {
      return '';
    }
    return $this->connector->getSolrBranch($version);
  }

  /**
   * {@inheritdoc}
   */
  public function getLuceneMatchVersion($version = '') {
    if (empty($this->connector)) {
      return '';
    }
    return $this->connector->getLuceneMatchVersion($version);
  }

  /**
   * {@inheritdoc}
   */
  public function getServerInfo($reset = FALSE) {
    if (empty($this->connector)) {
      throw new SearchApiSolrException;
    }
    return $this->connector->getServerInfo($reset);
  }

  /**
   * {@inheritdoc}
   */
  public function getCoreInfo($reset = FALSE) {
    if (empty($this->connector)) {
      throw new SearchApiSolrException;
    }
    return $this->connector->getCoreInfo($reset);
  }

  /**
   * {@inheritdoc}
   */
  public function getLuke() {
    if (empty($this->connector)) {
      throw new SearchApiSolrException;
    }
    return $this->connector->getLuke();
  }

  /**
   * {@inheritdoc}
   */
  public function getSchemaVersionString($reset = FALSE) {
    if (empty($this->connector)) {
      return '';
    }
    return $this->connector->getSchemaVersionString($reset);
  }

  /**
   * {@inheritdoc}
   */
  public function getSchemaVersion($reset = FALSE) {
    if (empty($this->connector)) {
      return '';
    }
    return $this->connector->getSchemaVersion($reset);
  }

  /**
   * {@inheritdoc}
   */
  public function pingCore() {
    if (empty($this->connector)) {
      return FALSE;
    }
    return $this->connector->pingCore();
  }

  /**
   * {@inheritdoc}
   */
  public function pingServer() {
    if (empty($this->connector)) {
      return FALSE;
    }
    return $this->connector->pingServer();
  }

  /**
   * {@inheritdoc}
   */
  public function getStatsSummary() {
    if (empty($this->connector)) {
      throw new SearchApiSolrException;
    }
    return $this->connector->getStatsSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function coreRestGet($path) {
    if (empty($this->connector)) {
      return '';
    }
    return $this->connector->coreRestGet($path);
  }

  /**
   * {@inheritdoc}
   */
  public function coreRestPost($path, $command_json = '') {
    if (empty($this->connector)) {
      return '';
    }
    return $this->connector->coreRestPost($path, $command_json);
  }

  /**
   * {@inheritdoc}
   */
  public function serverRestGet($path) {
    if (empty($this->connector)) {
      return '';
    }
    return $this->connector->serverRestGet($path);
  }

  /**
   * {@inheritdoc}
   */
  public function serverRestPost($path, $command_json = '') {
    if (empty($this->connector)) {
      return '';
    }
    return $this->connector->serverRestPost($path, $command_json);
  }

  /**
   * {@inheritdoc}
   */
  public function getUpdateQuery() {
    if (empty($this->connector)) {
      return (new Client())->createUpdate();
    }
    return $this->connector->getUpdateQuery();
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectQuery() {
    if (empty($this->connector)) {
      return (new Client())->createSelect();
    }
    return $this->connector->getSelectQuery();
  }

  /**
   * {@inheritdoc}
   */
  public function getMoreLikeThisQuery() {
    if (empty($this->connector)) {
      return (new Client())->createMoreLikeThis();
    }
    return $this->connector->getMoreLikeThisQuery();
  }

  /**
   * {@inheritdoc}
   */
  public function getTermsQuery() {
    if (empty($this->connector)) {
      return (new Client())->createTerms();
    }
    return $this->connector->getTermsQuery();
  }

  /**
   * {@inheritdoc}
   */
  public function getExtractQuery() {
    if (empty($this->connector)) {
      return (new Client())->createExtract();
    }
    return $this->connector->getExtractQuery();
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryHelper(QueryInterface $query = NULL) {
    if (!empty($this->connector)) {
      return $this->connector->getQueryHelper($query);
    }
    if ($query) {
      return $query->getHelper();
    }
    return new Helper();
  }

  /**
   * {@inheritdoc}
   */
  public function search(Query $query, Endpoint $endpoint = NULL) {
    return $this->connector->search($query, $endpoint);
  }

  /**
   * {@inheritdoc}
   */
  public function createSearchResult(Query $query, Response $response) {
    return $this->connector->createSearchResult($query, $response);
  }

  /**
   * {@inheritdoc}
   */
  public function update(UpdateQuery $query, Endpoint $endpoint = NULL) {
    // Although SolrConnectorInterface doesn't specify this can throw
    // an exception, this is what SolrConnectorPluginBase does.
    if (empty($this->connector)) {
      throw new SearchApiSolrException;
    }
    return $this->connector->update($query, $endpoint);
  }

  /**
   * {@inheritdoc}
   */
  public function execute(QueryInterface $query, Endpoint $endpoint = NULL) {
    if (empty($this->connector)) {
      throw new SearchApiSolrException;
    }
    return $this->connector->execute($query, $endpoint = NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function executeRequest(Request $request, Endpoint $endpoint = NULL) {
    if (empty($this->connector)) {
      throw new SearchApiSolrException;
    }
    return $this->connector->executeRequest($request, $endpoint = NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function optimize(Endpoint $endpoint = NULL) {
    if (empty($this->connector)) {
      return;
    }
    return $this->connector->optimize($endpoint);
  }

  /**
   * {@inheritdoc}
   */
  public function extract(QueryInterface $query) {
    // Although SolrConnectorInterface doesn't specify this can throw
    // an exception, this is what SolrConnectorPluginBase does.
    if (empty($this->connector)) {
      throw new SearchApiSolrException;
    }
    return $this->connector->extract($query);
  }

  /**
   * {@inheritdoc}
   */
  public function getContentFromExtractResult(ExtractResult $result, $filepath) {
    if (empty($this->connector)) {
      return '';
    }
    return $this->connector->getContentFromExtractResult($result, $filepath);
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoint($key = 'core') {
    if (empty($this->connector)) {
      return (new Client())->getEndpoint($key);
    }
    return $this->connector->getEndpoint($key);
  }

  /**
   * {@inheritdoc}
   */
  public function getFile($file = NULL) {
    // Although SolrConnectorInterface doesn't specify this can throw
    // an exception, this is what SolrConnectorPluginBase does.
    if (empty($this->connector)) {
      throw new SearchApiSolrException;
    }
    return $this->connector->getFile($file);
  }

  /**
   * {@inheritdoc}
   */
  public function viewSettings() {
    if (empty($this->connector)) {
      return [];
    }
    return $this->connector->viewSettings();
  }

}
